from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    npm = models.BigIntegerField()
    date_of_birth = models.DateField()
