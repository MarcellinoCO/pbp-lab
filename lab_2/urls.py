from django.urls import path
from .views import index, json, xml

urlpatterns = [
  path('', index, name='index'),
  path('json', json),
  path('xml', xml),
]
