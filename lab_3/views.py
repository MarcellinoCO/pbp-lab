from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

from lab_1.models import Friend
from lab_3.forms import FriendForm


@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {"friends": friends}
    return render(request, "lab3_index.html", response)


@login_required(login_url="/admin/login/")
def add_friend(request):
    form = FriendForm(request.POST or None)
    response = {"form": form}

    if request.method != "POST":
        return render(request, "lab3_form.html", response)

    if form.is_valid():
        form.save()
        return redirect("/lab-3")

    return render(request, "lab3_form.html", response)
