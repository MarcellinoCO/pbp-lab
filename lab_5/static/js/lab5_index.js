const createTableRow = ({ addressee, addresser, title, message }) => {
  const actionButtons = $("");

  const row = $("<tr></tr>").append(
    $("<td></td>").text(addressee),
    $("<td></td>").text(addresser),
    $("<td></td>").text(title),
    $('<td class="user-select-all"></td>').text(message)
  );

  $("#table").append(row);
};

$.get("/lab-2/json", (data) => {
  data.forEach((element) => {
    createTableRow({ ...element.fields });
  });
});

{
  /* <tbody id="table">
<!-- {% for note in notes %}
<tr>
  <td>{{ note.addressee }}</td>
  <td>{{ note.addresser }}</td>
  <td>{{ note.title }}</td>
  <td class="user-select-all">{{ note.message }}</td>
  <td class="d-flex flex-col justify-content-evenly">
    <button type="button" class="btn btn-primary">View</button>
    <button type="button" class="btn btn-outline-secondary">
      Edit
    </button>
    <button type="button" class="btn btn-outline-danger">
      Delete
    </button>
  </td>
</tr>
{% endfor %} -->
</tbody> */
}
