# 📝 Lab 2 Answers

### 1. Apakah perbedaan antara JSON dan XML?

Keduanya merupakan format yang populer digunakan dalam melakukan strukturisasi data.

JSON merupakan singkatan dari `JavaScript Object Notation` dan pertama kali digunakan dalam bahasa pemrograman Javascript sebagai notasi *object*-nya. 
Sedangkan, XML merupakan singkatan dari `eXtensible Markup Language` dan merupakan perbaruan dari SGML.

|Perbedaan|**JSON**|**XML**|
|---|---|---|
|Model penyimpanan data|Model *key value*|Model *tree structure*|
|Ukuran dokumen|Kecil dan sederhana|Besar dan struktur tag rumit|
|Kecepatan *parsing*|Sangat cepat|Lambat|
|Value processing|Tidak ada|Pemrosesan dan pemformatan objek|
|Supported data types|Hanya primitif|Primitif dan non-primitif|
|Supported namespaces|Tidak ada|Namespace, comment, metadata|
|Array type support|Array|Tidak ada|

### 2. Apakah perbedaan antara HTML dan XML?

Keduanya sama-sama merupakan Markup Language, namun memiliki kegunaan yang sangat berbeda.

HTML (`HyperText Markup Language`) banyak digunakan untuk keperluan antarmuka halaman web.
Sedangkan XML (`eXtensible Markup Language`) banyak digunakan untuk keperluan transimi data.

|Perbedaan|**HTML**|**XML**|
|---|---|---|
|Fokus|Penyajian data|Transfer data|
|Case sensitivity|Case insensitive|Case sensitive|
|Whitespace|Tidak disimpan|Disimpan|
|Tag penutup|Tidak strict|Strict|
|Jenis tag|Terbatas|Dapat dikembangkan|
