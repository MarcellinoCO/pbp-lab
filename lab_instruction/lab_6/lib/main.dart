import 'package:flutter/material.dart';

import './screens/main_screen.dart';
import './screens/checklist_screen.dart';

void main() => runApp(TemeninIsomanApp());

class TemeninIsomanApp extends StatefulWidget {
  @override
  _TemeninIsomanAppState createState() => _TemeninIsomanAppState();
}

class _TemeninIsomanAppState extends State<TemeninIsomanApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Temenin Isoman',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.yellow,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      initialRoute: '/',
      routes: {
        '/': (ctx) => MainScreen(),
        ChecklistScreen.routeName: (ctx) => ChecklistScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
        return null;
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => MainScreen(),
        );
      },
    );
  }
}
