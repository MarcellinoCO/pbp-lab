import 'package:flutter/material.dart';

class ChecklistScreen extends StatelessWidget {
  static const routeName = '/checklist';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Checklist'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
            ),
          ],
        ),
      ),
    );
  }
}
